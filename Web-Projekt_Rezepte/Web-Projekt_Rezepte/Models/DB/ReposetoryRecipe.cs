﻿using System;
using System.Data;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Web.WebPages.Razor;




namespace WebProjekt_Rezepte.Models.DB
{
    public class ReposetoryRecipe
    {
		private string _connectionString = "Server=localhost; Database=web; UID=root; Pwd=Ghost3131";
		private MySqlConnection _connection;

		public void Open()
		{
			if (this._connection == null)
			{
				this._connection = new MySqlConnection(this._connectionString);
			}
			if (this._connection.State != ConnectionState.Open)
			{
				this._connection.Open();
			}
		}

		public void Close()
		{
			if ((this._connection != null) && (this._connection.State == ConnectionState.Open))
			{
				this._connection.Close();
			}
		}
    }
}
